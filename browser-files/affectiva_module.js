var detector = null;
var camfeed = null;
var width = 640;
var height = 480;
var canvas = null;
var ctx = null;
var startTimeStamp = null;
var detector = null;

$(document).ready(function(){
    var faceMode = affdex.FaceDetectorMode.LARGE_FACES;
    detector = new affdex.FrameDetector(faceMode);
    
    detector.detectAllEmotions();
    //detector.detectAllExpressions();
    //detector.detectAllEmojis();
    //detector.detectAllAppearance();
    
    detector.addEventListener("onInitializeSuccess", function() {
        console.log('Detector reports initialized.');
        window.detectorStatus = true;
        //processImage();
    });
    
    detector.addEventListener("onImageResultsSuccess", function(faces, image, timestamp) {
        if (faces.length > 0) {
            console.log("Image successfully recognized!")
            valence = faces[0].emotions.valence;
            console.log("results-valence: "+valence);
       
            window.java.setValence(valence,timestamp);
            //processImage();
        }
    });
    
    detector.addEventListener("onImageResultsFailure", function (image, timestamp, err_detail) {
        console.log(err_detail);
        window.java.removeImage();
        //processImage();
    });
    
    canvas = document.getElementById("mycanvas");
    ctx = canvas.getContext("2d");
    
    startTimeStamp = (new Date()).getTime() / 1000;
    if (detector && !detector.isRunning) {
        $("#logs").html("");
        detector.start();
        console.log("Starting detector");
    }
});
  
function log(node_name, msg) {
    $(node_name).append("<span>" + msg + "</span><br />");
}

function processImage() {
    console.log("entered processImage");
    var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var currentTimeStamp = (new Date()).getTime() / 1000;
    detector.process(imgData, currentTimeStamp - startTimeStamp);
    return true;
}

function updateCanvasImage() {
    console.log("Updating canvas image");
    
    if(!canvas) {
        canvas = document.getElementById("mycanvas");
    }
    if(!ctx) {
        ctx = canvas.getContext("2d");
    }
    var imageData = ctx.getImageData(0,0,canvas.width, canvas.height);
    var data = imageData.data;
    
    for (var i = 0; i < data.length; i += 4) {
        data[i]     = window.imgArray[i];     // red
        data[i + 1] = window.imgArray[i + 1]; // green
        data[i + 2] = window.imgArray[i + 2]; // blue
        data[i + 3] = window.imgArray[i + 3]; // alpha
    }
    ctx.putImageData(imageData, 0, 0);
    processImage();
    return true;
}