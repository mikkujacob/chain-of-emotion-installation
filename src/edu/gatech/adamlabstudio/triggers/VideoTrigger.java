package edu.gatech.adamlabstudio.triggers;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Random;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.video.Movie;


/**
 * Class used to play video triggers with emotionally valenced and arousing content.
 * @author mikhail.jacob
 *
 */
public class VideoTrigger
{
	/**
	 * Test mode generates random frames for testing and does not actually play video.
	 */
	private final boolean isTestMode = false;
	
	/**
	 * Object reference to the main Processing sketch object.
	 */
	private PApplet papplet;
	
	/**
	 * Object to store rendered video trigger frames.
	 */
	private PGraphics videoGraphics;
	
	/**
	 * Object to generate random data.
	 */
	private Random random;
	
	/**
	 * Location of positive video triggers.
	 */
	private final String positiveTriggerPath = "data" + File.separator + "video-triggers" + File.separator + "positive";
	
	/**
	 * List of positive video trigger filenames found on searching directory "triggerPath".
	 */
	private String[] positiveTriggerNames;
	
	/**
	 * Index of currently cued positive trigger video.
	 */
	private int currentPositiveTriggerIndex;
	
	/**
	 * Location of negative video triggers.
	 */
	private final String negativeTriggerPath = "data" + File.separator + "video-triggers" + File.separator + "negative";
	
	/**
	 * List of negative video trigger filenames found on searching directory "triggerPath".
	 */
	private String[] negativeTriggerNames;
	
	/**
	 * Index of currently cued negative trigger video.
	 */
	private int currentNegativeTriggerIndex;
	
	/**
	 * Movie trigger object reference to store the current video trigger.
	 */
	private Movie currentTrigger;
	
	/**
	 * Whether trigger video is currently playing or not.
	 */
	private boolean isPlaying;
	
	/**
	 * Whether trigger video is currently cued or not.
	 */
	private boolean isCued;
	
	/**
	 * Parameterized constructor for this class.
	 * @param papplet - Main PApplet object passed in. 
	 */
	public VideoTrigger(PApplet papplet)
	{
		this.papplet = papplet;
		videoGraphics = papplet.createGraphics(papplet.displayWidth, papplet.displayHeight);
		random = new Random();
		boolean triggersLoaded = true;
		isCued = false;
		isPlaying = false;
		positiveTriggerNames = loadTriggersFromPath(positiveTriggerPath);
		if(positiveTriggerNames.length == 0)
		{
			System.out.println("No Positive Trigger Videos Found In " + positiveTriggerPath);
			triggersLoaded = false;
		}
		negativeTriggerNames = loadTriggersFromPath(negativeTriggerPath);
		if(negativeTriggerNames.length == 0)
		{
			System.out.println("No Negative Trigger Videos Found In " + negativeTriggerPath);
			triggersLoaded = false;
		}
		if(triggersLoaded)
		{
			currentPositiveTriggerIndex = currentNegativeTriggerIndex = 0;
			cueNextVideoClip(random.nextBoolean());
		}
	}
	
	private String[] loadTriggersFromPath(String triggerPath)
	{
		File path = new File(triggerPath);
		return path.list(new FilenameFilter()
		{
			@Override
			public boolean accept(File dir, String name)
			{
//				return ((name.endsWith(".mp4")) ? true : false);
				return ((name.endsWith(".mov")) ? true : false);
			}
		});
	}
	
	/**
	 * Method used to draw next frame of content.
	 */
	public void drawNextFrame()
	{
		if(!isTestMode)
		{
			drawNextVideoTriggerFrame();
		}
		else
		{
			generateTestVideoFrame();
		}
		
		papplet.image(videoGraphics, 0, 0, videoGraphics.width, videoGraphics.height);
	}
	
	public boolean cueNextVideoClip(boolean isPositive)
	{
		boolean loadSucceeded = false;
		if(isPositive)
		{
			if(positiveTriggerNames.length == 0)
			{
				return false;
			}
			currentPositiveTriggerIndex = (currentPositiveTriggerIndex + 1) % positiveTriggerNames.length;
			System.out.println("Current positive video: " + positiveTriggerPath + File.separator + positiveTriggerNames[currentPositiveTriggerIndex]);
			currentTrigger = new Movie(papplet, positiveTriggerPath + File.separator + positiveTriggerNames[currentPositiveTriggerIndex]);
		}
		else
		{
			if(negativeTriggerNames.length == 0)
			{
				return false;
			}
			currentNegativeTriggerIndex = (currentNegativeTriggerIndex + 1) % negativeTriggerNames.length;
			System.out.println("Current negative video: " + negativeTriggerPath + File.separator + negativeTriggerNames[currentNegativeTriggerIndex]);
			currentTrigger = new Movie(papplet, negativeTriggerPath + File.separator + negativeTriggerNames[currentNegativeTriggerIndex]);
		}
		if(loadSucceeded)
		{
			playLooping();
			isCued = true;
		}
		return loadSucceeded;
	}
	
	private void drawNextVideoTriggerFrame()
	{
		videoGraphics.beginDraw();
		videoGraphics.background(0);
		if(currentTrigger != null)
		{
//			System.out.println("Frame");
			if(currentTrigger.available())
			{
				currentTrigger.read();
//				System.out.println("Available Frame");
			}
			videoGraphics.image(currentTrigger.get(), 0, 0, videoGraphics.width, videoGraphics.height);
		}
		videoGraphics.endDraw();
	}
	
	/**
	 * Method used to generate a frame for display.
	 */
	private void generateTestVideoFrame()
	{
		videoGraphics.beginDraw();
		int oldFill = videoGraphics.fillColor;
		int fillColor = videoGraphics.color(random.nextInt(256), random.nextInt(256), random.nextInt(256), 128 + random.nextInt(128));
		videoGraphics.fill(fillColor);
		int x1 = random.nextInt(videoGraphics.width),
				y1 = random.nextInt(videoGraphics.height),
				x2 = x1 + random.nextInt(videoGraphics.width - x1),
				y2 = y1 + random.nextInt(videoGraphics.height - y1);
		videoGraphics.rect(x1, y1, x2, y2);
		videoGraphics.fill(oldFill);
		videoGraphics.endDraw();
	}
	
	public void play()
	{
		if(currentTrigger != null)
		{
			currentTrigger.play();
			isPlaying = true;
		}
	}
	
	public void pause()
	{
		if(currentTrigger != null)
		{
			currentTrigger.pause();
			isPlaying = false;
		}
	}
	
	public void stop()
	{
		if(currentTrigger != null)
		{
			currentTrigger.stop();
			isPlaying = false;
		}
	}
	
	public void playLooping()
	{
		if(currentTrigger != null)
		{
			currentTrigger.loop();
			isPlaying = true;
		}
	}
	
	public boolean isPlaying()
	{
		return isPlaying;
	}
	
	public boolean isCued()
	{
		return isCued;
	}
	
	public void setCued(boolean isCued)
	{
		this.isCued = isCued;
	}
}
