package edu.gatech.adamlabstudio.transference;

import edu.gatech.adamlabstudio.application.Contagion.ConfigurationType;
import edu.gatech.adamlabstudio.transference.io.syphon.SyphonClient;
import edu.gatech.adamlabstudio.transference.io.syphon.SyphonServer;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import spout.Spout;

/**
 * Class used to send, receive, and draw streaming video. Class uses Syphon on macOS and Spout on Windows
 * @author mikhail.jacob
 *
 */
public class VideoStreaming
{
	/**
	 * Object reference to the main Processing sketch object.
	 */
	private PApplet papplet;
	
	/**
	 * String indicating the receiving server name to receive video buffer data.
	 */
	private static String receptionServerName = "Contagion";
	
	/**
	 * String indicating the transmitting application / class name to receive video buffer data.
	 */
	private static final String transmissionApplicationName = "TCPSyphonClient";
	
	/**
	 * String indicating the transmitting server name to transmit video buffer data.
	 */
	private static String transmissionServerName = "Contagion";
	
	/**
	 * OSType enumerator that describes whether the current OS is Windows or MacOS or Linux
	 */
	private static OSType osType;
	
	/**
	 * Object that sends video buffer content to a Syphon client.
	 */
	private SyphonServer syphonServer;
	
	/**
	 * Object that receives video buffer content from a Syphon server.
	 */
	private SyphonClient syphonClient;
	
	/**
	 * Object that receives and sends video buffer content from a Spout client or server.
	 */
	private Spout spout;
	
	/**
	 * Object to store received video buffer frame data.
	 */
	private PGraphics receivedGraphics;
	
	/**
	 * Object to store an image of the video buffer frame data.
	 */
	private PImage receivedImage;
	
	/**
	 * Parameterized constructor for this class.
	 * @param papplet - Main PApplet object passed in. 
	 */
	public VideoStreaming(PApplet papplet)
	{
		this.papplet = papplet;
		
		receivedGraphics = papplet.createGraphics(papplet.displayWidth, papplet.displayHeight, PApplet.P3D);
		
		initializeVideoStreaming();
	}
	
	private void initializeVideoStreaming()
	{
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1)  
		{
			osType = OSType.WINDOWS;
		}
		else if(System.getProperty("os.name").toLowerCase().indexOf("mac") > -1)
		{
			osType = OSType.MACOS;
		}
		else
		{
			osType = OSType.LINUX;
			//TODO Find a Linux solution to video transfer.
		}
		
		if(osType == OSType.MACOS)
		{
			if(syphonServer != null)
			{
				syphonServer.stop();
			}
			syphonServer = new SyphonServer(papplet, transmissionServerName);
			if(syphonClient != null)
			{
				syphonClient.stop();
			}
//			syphonClient = new SyphonClient(papplet, transmissionApplicationName, receptionServerName);
			syphonClient = new SyphonClient(papplet, transmissionApplicationName);
		}
		else if(osType == OSType.WINDOWS)
		{
			if(spout != null)
			{
				spout.release();
			}
			spout = new Spout(papplet);
			spout.createSender(transmissionServerName);
			spout.createReceiver(receptionServerName);
		}
	}
	
	/**
	 * Method to receive video buffer frames
	 */
	public void receiveVideo()
	{
		if(osType == OSType.MACOS)
		{
			if(syphonClient == null)
			{
//				syphonClient = new SyphonClient(papplet, transmissionApplicationName, receptionServerName);
				syphonClient = new SyphonClient(papplet, transmissionApplicationName);
			}
			
			if(syphonClient.newFrame())
			{
				receivedGraphics = syphonClient.getGraphics(receivedGraphics);
				receivedImage = receivedGraphics.get();
			}
		}
		else if(osType == OSType.WINDOWS)
		{
			if(spout == null)
			{
				spout = new Spout(papplet);
				spout.createSender(transmissionServerName);
				spout.createReceiver(receptionServerName);
			}
			receivedGraphics = spout.receiveTexture(receivedGraphics);
			receivedImage = receivedGraphics.get();
		}
	}
	
	/**
	 * Method to draw video buffer frames on screen
	 */
	public void drawVideo()
	{
		papplet.background(0);
		if(receivedImage != null)
		{
			papplet.image(receivedImage, 0, 0, receivedImage.width, receivedImage.height);
		}
	}
	
	/**
	 * Method to send video buffer frames
	 */
	public void streamVideo()
	{
		if(osType == OSType.MACOS)
		{
			if(syphonServer == null)
			{
				syphonServer = new SyphonServer(papplet, transmissionServerName);
			}
			syphonServer.sendScreen();
		}
		else if(osType == OSType.WINDOWS)
		{
			if(spout == null)
			{
				spout = new Spout(papplet);
				spout.createSender(transmissionServerName);
				spout.createReceiver(receptionServerName);
			}
			spout.sendTexture();
		}
	}
	
	public void setConfiguration(ConfigurationType configuration)
	{
//		switch(configuration)
//		{
//			case A:
//			{
//				transmissionServerName = "Contagion#1";
//				receptionServerName = "Contagion#2";
//				break;
//			}
//			case B:
//			{
//				transmissionServerName = "Contagion#2";
//				receptionServerName = "Contagion#1";
//				break;
//			}
//			default:
//			{
//				transmissionServerName = "Contagion#1";
//				receptionServerName = "Contagion#2";
//			}
//		}
//		
//		initializeVideoStreaming();
	}
	
	public enum OSType
	{
		WINDOWS,
		MACOS,
		LINUX
	}
}
