package edu.gatech.adamlabstudio.transference.io.Kinect;

import org.openkinect.processing.Kinect;

import processing.core.PApplet;
import processing.core.PImage;

public class KinectInterface
{
	private PApplet papplet;
	private Kinect kinect;
	private PImage depthFrame;
	private PImage irFrame;
	
	private boolean isDepth = false;
	
	public KinectInterface(PApplet papplet)
	{
		this.papplet = papplet;
		isDepth = false;
		try
		{
			kinect = new Kinect(papplet);
			if(kinect != null)
			{
				kinect.initDepth();
				kinect.initVideo();
				kinect.enableIR(true);
			    kinect.enableMirror(true);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void update()
	{
		if(kinect != null)
		{
			if(isDepth)
			{
				depthFrame = kinect.getDepthImage();
			}
			else
			{
				irFrame = kinect.getVideoImage();
			}
		}
	}
	
	public void drawImage()
	{
		papplet.background(0);
		if(isDepth)
		{
			if(depthFrame != null)
			{
				papplet.image(depthFrame, 0, 0, papplet.width, papplet.height);
			}
		}
		else
		{
			if(irFrame != null)
			{
				papplet.image(irFrame, 0, 0, papplet.width, papplet.height);
			}
		}
	}
	
	public PImage getDepthFrame()
	{
		return depthFrame;
	}
	
	public PImage getIRFrame()
	{
		return irFrame;
	}
	
	public PImage getCurrentFrame()
	{
		if(isDepth)
		{
			return depthFrame;
		}
		else
		{
			return irFrame;
		}
	}
	
	public boolean isDepth()
	{
		return isDepth;
	}
	
	public void setIsDepth(boolean isDepth)
	{
		this.isDepth = isDepth;
	}
	
	public void toggleIsDepth()
	{
		isDepth = !isDepth;
	}
}
