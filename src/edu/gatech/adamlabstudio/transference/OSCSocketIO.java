package edu.gatech.adamlabstudio.transference;

import edu.gatech.adamlabstudio.application.Contagion;
import edu.gatech.adamlabstudio.application.Contagion.ConfigurationType;
import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;

public class OSCSocketIO
{
	/**
	 * A reference to the parent sketch.
	 */
	private PApplet papplet;
	
	/**
	 * An object to transmit and receive messages over OSC protocol.
	 */
	private OscP5 osc;

	/**
	 * Port to receive from.
	 */
	private static int receptionPort = 40000;
	
	/**
	 * Address to transmit to.
	 */
	private NetAddress transmissionAddress;

	/**
	 * URL to transmit to.
	 */
	private static String transmissionURL = "127.0.0.1";

	/**
	 * Port to transmit to.
	 */
	private static int transmissionPort = 40000;

	/**
	 * Pattern to receive messages under.
	 */
	private static String pattern = "/control";

	/**
	 * Constructor to initialize class and sockets.
	 * @param papplet
	 */
	public OSCSocketIO(PApplet papplet)
	{
		this.papplet = papplet;
		initializeSockets();
	}
	
	/**
	 * Method to initialize sockets.
	 */
	private void initializeSockets()
	{
		if(osc != null)
		{
			osc.stop();
		}
		osc = new OscP5(papplet, receptionPort);
		transmissionAddress = new NetAddress(transmissionURL, transmissionPort);
		osc.plug(this, "handleControlMessage", pattern);
	}
	
	/**
	 * Method to send a string message using the current pattern string.
	 * @param message - The message string.
	 */
	public void sendMessage(String message)
	{
		sendMessage(pattern, message);
	}
	
	/**
	 * Method to send a string message using a pattern string. 
	 * @param pattern - The pattern string.
	 * @param message - The string message.
	 */
	public void sendMessage(String pattern, String message)
	{
		OscMessage oscMessage = new OscMessage(pattern);
		oscMessage.add(message);
		osc.send(oscMessage, transmissionAddress);
	}
	
//	/**
//	 * Method to handle any OSC Event for received message.
//	 * @param message - The OSC message.
//	 */
//	public void oscEvent(OscMessage message)
//	{
//		
//	}
	
	/**
	 * Method to handle a control message.
	 * @param message - The message string.
	 */
	public void handleControlMessage(String message)
	{
		try
		{
			ConfigurationType configuration = ConfigurationType.valueOf(message);
			Contagion.setConfiguration(configuration);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to set the configuration of the OSC sockets.
	 * @param configuration - The configuration type.
	 */
	public void setConfiguration(ConfigurationType configuration)
	{
//		switch(configuration)
//		{
//			case A:
//			{
//				receptionPort = 40000;
//				transmissionURL = "192.168.1.2";
//				transmissionPort = 40000;
//				pattern = "/controlAtoB";
//				break;
//			}
//			case B:
//			{
//				receptionPort = 40000;
//				transmissionURL = "192.168.1.2";
//				transmissionPort = 40000;
//				pattern = "/controlBtoA";
//				break;
//			}
//			default:
//			{
//				receptionPort = 40000;
//				transmissionURL = "192.168.1.2";
//				transmissionPort = 40000;
//				pattern = "/controlAtoB";
//			}
//		}
//		
//		initializeSockets();
	}
}
