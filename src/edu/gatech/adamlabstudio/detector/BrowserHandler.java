package edu.gatech.adamlabstudio.detector;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.LoggerProvider;
import com.teamdev.jxbrowser.chromium.events.ConsoleEvent;
import com.teamdev.jxbrowser.chromium.events.ConsoleListener;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.FrameLoadEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.events.LoadEvent;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

/**
 * This class encapsulates a JXBrowser instance and provides methods to interact with it
 */
public class BrowserHandler {
	
	private static String URL = null;
	private EmotionDetector main = null;
	private final Browser browser = new Browser();
	
	public BrowserHandler(String url, EmotionDetector emotionDetector) {
		URL = url;
		main = emotionDetector;
	}
	
	/**
	 * start jxbrowser window
	 */
	public void startBrowser() {
		// enable logging
		LoggerProvider.setLevel(Level.SEVERE);
		
		//start browser view
		BrowserView view = new BrowserView(browser);
		
		//set URL in address bar
		final JTextField addressBar = new JTextField(URL);
		
		//adding action listeners to address bar - for use if required later
        addressBar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                browser.loadURL(addressBar.getText());
                System.out.println("Action performed on address bar");
            }
        });
        
        //setting addressbar view properties
        JPanel addressPane = new JPanel(new BorderLayout());
        addressPane.add(new JLabel(" URL: "), BorderLayout.WEST);
        addressPane.add(addressBar, BorderLayout.CENTER);
        
        //add listeners and java callbacks to browser
        addLoadListener();
        addConsoleListener();
        
        //set browser frame properties
        JFrame frame = new JFrame("Emotion Detector");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(addressPane, BorderLayout.NORTH);
        frame.add(view, BorderLayout.CENTER);
        frame.setSize(800, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        //load browser document
        browser.loadURL(addressBar.getText());
        
        //DOMDocument document = browser.getDocument();

    }

	private void addConsoleListener() {
		System.out.println("Adding console listener");
		browser.addConsoleListener(new ConsoleListener() {
            public void onMessage(ConsoleEvent event) {
                System.out.println("Browser: " + event.getMessage());
            }
        });		
	}

	/**
	 * adds listeners to the jxbrowser to attach callbacks once document is loaded
	 */
	private void addLoadListener() {
		browser.addLoadListener(new LoadAdapter() {
            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent event) {
                if (event.isMainFrame()) {
                    System.out.println("Main frame has finished loading");
                    Browser browser = event.getBrowser();
                    JSValue value = browser.executeJavaScriptAndReturnValue("window");
                    String property = "java";
                    value.asObject().setProperty(property, main);
                    value.asObject().setProperty("detectorStatus", EmotionDetector.detectorInitialized);
                } else {
                	System.out.println("NOT MAIN FRAME");
                }
            }

            @Override
            public void onDocumentLoadedInFrame(FrameLoadEvent event) {
                System.out.println("Frame document is loaded.");
            }

            @Override
            public void onDocumentLoadedInMainFrame(LoadEvent event) {
                System.out.println("Main frame document is loaded.");   
            }
        });		
	}
	
	public Browser getBrowser() {
		return browser;
	}
}
