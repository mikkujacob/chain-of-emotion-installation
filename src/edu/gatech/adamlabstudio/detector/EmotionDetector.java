package edu.gatech.adamlabstudio.detector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.JSArray;
import com.teamdev.jxbrowser.chromium.JSONString;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;

import edu.gatech.adamlabstudio.utilities.VideoFrame;
import processing.core.PApplet;
import processing.core.PImage;
import processing.data.JSONArray;

public class EmotionDetector
{
	
	public static boolean detectorInitialized = false;
	/**
	 * Static int specifying threshold for considering an emotion signficant
	 */
	private static int emoThreshold = 20;
	
	/**
	 * Parent PApplet reference.
	 */
	private PApplet papplet;
	
	/**
	 * Object to interface with JxBrowser and Javascript.
	 */
	private Browser browser;
	
	/**
	 * String URL for the Affectiva Javascript API.
	 */
	private String URL = "http://localhost:8000";
	
	/**
	 * ArrayList to store rolling list of video images to transfer over network.
	 */
	private ArrayList<PImage> images;
	
	/**
	 * ArrayList to store rolling list of video frames to transfer over network.
	 */
	private ArrayList<VideoFrame> videoFrames;
	
	/**
	 * ArrayList to store video frames of interesting emotion to transfer over network.
	 */
	private ArrayList<VideoFrame> transferVideo;
	
	/**
	 * Total size of the video frames list. 
	 */
	private int framesSize = 30 * 15;
	
	/**
	 * Total size to consider within that video frames list.
	 */
	private int windowSize = 30 * 10;
	
	/**
	 * Constructor to initialize the class.
	 * @param papplet
	 */
	public EmotionDetector(PApplet papplet)
	{
		this.papplet = papplet;
		videoFrames = new ArrayList<VideoFrame>();
		images = new ArrayList<PImage>();
		
		BrowserHandler browserHandler = new BrowserHandler(URL,this);
		browserHandler.startBrowser();
		browser = browserHandler.getBrowser();
	}
	
	/**
	 * Method to process a video frame for emotion detection. 
	 * @param frame - The video frame to process (PImage)
	 */
	public void processFrame(PImage frame)
	{
		images.add(frame);
		
		frame.loadPixels();
	    int[] pixelArray = frame.pixels;
	    
	    int[] arg = convert2rgb(pixelArray);
	    
	    setImageInJX(arg); 
	}
	
	/**
	 * Method to update canvas image in the browser.
	 * @return
	 */
	private void setImageInJX(int[] arg) 
	{
		 JSONArray jsonarray = new JSONArray();
		 for (int i = 0; i < arg.length; i++) {
		     jsonarray.append(arg[i]);
		 }
		 String param = jsonarray.toString();
		 
		 JSValue window = browser.executeJavaScriptAndReturnValue("window");
		 window.asObject().setProperty("imgArray",new JSONString(param));
		 
		 JSValue result = browser.executeJavaScriptAndReturnValue("updateCanvasImage();");
//		 System.out.println(result.getBooleanValue());		
	}

	/**
	 * Method to return a string representation of a JS Array consisting of RGBA pixel information
	 * @return
	 */
	private int[] convert2rgb(int[] pixelArray) 
	{
		int len = pixelArray.length;
		int[] arg = new int[len*4];
		for(int i =0;i<len;i++) {
			arg[i] = (int) Math.rint(papplet.red(pixelArray[i]));
			arg[i+1] = (int) Math.rint(papplet.green(pixelArray[i]));
			arg[i+2] = (int) Math.rint(papplet.blue(pixelArray[i]));
			arg[i+3] = (int) Math.rint(papplet.alpha(pixelArray[i]));
		}
		return arg;
	}
	
	/**
	 * Method called from jxbrowser object to transfer valence,timestamp
	 * @param valence
	 * @param timestamp
	 */
	public void setValence(int valence, int timestamp) 
	{
        System.out.println("printing valence: "+valence+","+timestamp);
        
        PImage img = images.remove(0);
        VideoFrame frame = new VideoFrame(img, timestamp, valence);
        
        boolean isBufferFull = (videoFrames.size() == framesSize);
        if(isBufferFull)
		{
			videoFrames.remove(0);
		}
		videoFrames.add(frame);
		System.out.println("Added frame");
		
		if(isBufferFull) {
			checkExtremeEmotion();
		}
    }
	
	public void removeImage() {
		images.remove(0);
	}
	
	public void saveImage() {
		JSValue value = browser.executeJavaScriptAndReturnValue("getImage()");
		//below line of code buggy!!
		JSArray array = value.asObject().asArray();
		PImage img = papplet.createImage(640,480,PApplet.ARGB);
		img.loadPixels();
		for(int i=0;i<array.length();i+=4) {
			int pixel = papplet.color(array.get(i).asNumber().getInteger(), array.get(i+1).asNumber().getInteger(), array.get(i+2).asNumber().getInteger(), array.get(i+3).asNumber().getInteger());
			img.pixels[i/4] = pixel;
		}
		images.add(img);
	}
	
	/**
	 * Method to inspect images in videoFrames and detect high valenced frame spikes. 
	 * @return
	 */
	private void checkExtremeEmotion() 
	{	
		Map<Integer, Integer> valenceMap = new HashMap<Integer, Integer>();
		
		int avg = 0;
		int sum = 0;
		int valence = 0;
		for (int i=0; i<videoFrames.size(); i++) {
			valence = videoFrames.get(i).getValence();
			valenceMap.put(i,valence);
			sum += valence;
		}
		avg = sum/videoFrames.size();
		
		int max = 0;
		int diff = 0;
		int curDiff = 0;
		for (int i=0; i<valenceMap.size(); i++) {
			curDiff = Math.abs(avg-valenceMap.get(i));
			if(curDiff>diff) {
				diff = curDiff;
				max = i;
			}
		}
		if(diff>emoThreshold) {
			storeNewClip(max);
		}
	}

	/**
	 * Method to store a list of video frames of highly valenced frame from this class 
	 * @return
	 */
	public void storeNewClip(int i)
	{
		int j;
		transferVideo.clear();
		if (i<framesSize/3) {
			for(j = 0; j<windowSize; j++) {
				transferVideo.add(videoFrames.get(j));
			}
		} else if(i>framesSize*2/3) {
			for(j = framesSize/3; j<framesSize; j++) {
				transferVideo.add(videoFrames.get(j));
			}
		} else if (i-windowSize/2>0){
			for(j = i-windowSize/2; j<i+windowSize/2; j++) {
				transferVideo.add(videoFrames.get(j));
			}
		}
		//add call to send video
	}
	
	/**
	 * Method to get a list of video frames from this class to transfer over network
	 * @return
	 */
	public ArrayList<VideoFrame> getTransferClip()
	{
		return transferVideo;
	}
}
