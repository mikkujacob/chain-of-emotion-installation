package edu.gatech.adamlabstudio.application.gui;

import java.util.ArrayList;

import processing.core.PApplet;

public class GUI
{
	private PApplet papplet;
	private ArrayList<String> listGUIText = new ArrayList<String>();
	private ArrayList<Long> listGUIDuration = new ArrayList<Long>();
	private boolean isTextDisplayed = true;

	public GUI(PApplet papplet)
	{
		this.papplet = papplet;
	}

	/*
	 * Displays a string message on the frontend screen during displayGUI() for
	 * a long duration in milliseconds.
	 */
	public void addGUIText(String message, long duration)
	{
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String testMessage = listGUIText.get(index);
			if (message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0]))
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}

		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}

	/*
	 * Displays tHe items in listGUIText for the duration specified in
	 * addGUIText()
	 */
	public void displayGUI()
	{
		if(!isTextDisplayed)
		{
			return;
		}
		papplet.pushMatrix();
		int oldFill = papplet.g.fillColor;
		papplet.fill(255);
		papplet.textAlign(PApplet.LEFT, PApplet.TOP);
		papplet.textSize(40);
		long timeNowMillis = System.currentTimeMillis();

		float xPosPercent = 4f / 1000f * (float) papplet.displayWidth;
		float yPosPercent = 3f / 1000f * (float) papplet.displayHeight;

		yPosPercent += 5f / 100f * (float) papplet.displayHeight;
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			papplet.text(message, xPosPercent, yPosPercent, 1.7f);
//			System.out.println("X: " + xPosPercent);
//			System.out.println("Y: " + yPosPercent);
			yPosPercent += 5f / 100f * (float) papplet.displayHeight;
			if (displayTime < timeNowMillis)
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
		}
		papplet.fill(oldFill);
		papplet.popMatrix();
	}

	/**
	 * @return the isTextDisplayed
	 */
	public boolean isTextDisplayed()
	{
		return isTextDisplayed;
	}

	/**
	 * @param isTextDisplayed the isTextDisplayed to set
	 */
	public void setTextDisplayed(boolean isTextDisplayed)
	{
		this.isTextDisplayed = isTextDisplayed;
	}
	
	public void toggleTestDisplayed()
	{
		this.isTextDisplayed = !isTextDisplayed;
	}
}
