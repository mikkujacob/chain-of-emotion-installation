package edu.gatech.adamlabstudio.application;

import java.util.Random;

import edu.gatech.adamlabstudio.application.gui.GUI;
import edu.gatech.adamlabstudio.detector.EmotionDetector;
import edu.gatech.adamlabstudio.transference.OSCSocketIO;
import edu.gatech.adamlabstudio.transference.VideoStreaming;
import edu.gatech.adamlabstudio.transference.io.Kinect.KinectInterface;
import edu.gatech.adamlabstudio.triggers.VideoTrigger;
import processing.core.PApplet;
import processing.opengl.PJOGL;

/**
 * Main class inherited from Processing sketch class (PApplet) where execution
 * of the application starts.
 * 
 * @author Mikhail, Soumya
 *
 */
public class Contagion extends PApplet
{
	/**
	 * Flag to display sketch fullscreen or not.
	 */
	private static final Boolean isFullScreen = true;
	
	/**
	 * Which configuration this application is currently in.
	 */
	private static ConfigurationType configuration = ConfigurationType.A;

	/**
	 * Which configuration was this application using before any change.
	 */
	private ConfigurationType lastConfiguration = ConfigurationType.A;
	
	/**
	 * Object to generate random numbers or variable values.
	 */
	private Random random;
	
	/**
	 * Boolean that describes whether video buffer content is being received or not.
	 */
	private boolean isVideoReceiving = true;
	
	/**
	 * Enum to choose the type of the video frame currently being drawn on screen.
	 */
	private VideoFrameType videoFrameType = VideoFrameType.VIDEO_TRIGGER;
	
	/**
	 * Boolean that describes whether video buffer content is being transmitted or not.
	 */
	private boolean isVideoTransmitting = true;
	
	/**
	 * Object used to send, receive, and draw video buffer content.
	 */
	private VideoStreaming videoStreaming;
	
	/**
	 * Object used to choose and play video triggers with emotional valence and arousal.
	 */
	private VideoTrigger videoTrigger;
	
	/**
	 * Object use to detect emotion and automatically choose the moment of strong valence.
	 */
	private EmotionDetector emotionDetector;
	
	/**
	 * Object used to interface with the Kinect and get depth and IR images.
	 */
	private KinectInterface kinectInterface;
	
	/**
	 * Object used to send and receive control messages over OSC protocol.
	 */
	private OSCSocketIO oscSocketIO;
	
	/**
	 * Object used to display GUI and text associated with the display.
	 */
	private GUI gui;
	
	/**
	 * Method where execution starts.
	 * 
	 * @param args - Command line arguments if any.
	 */
	public static void main(String[] args)
	{
		if (isFullScreen)
		{
			PApplet.main(new String[] { "--present",
					"edu.gatech.adamlabstudio.application.Contagion" });
		}
		else
		{
			PApplet.main(new String[] { "edu.gatech.adamlabstudio.application.Contagion" });
		}
	}

	/**
	 * Processing method called once at the very beginning to set sketch
	 * settings.
	 */
	public void settings()
	{
		if (isFullScreen)
		{
			// window size
			size(displayWidth, displayHeight, P3D);
		}
		else
		{
			// window size
			size(600, 600, P3D);
		}
		
		PJOGL.profile = 1;

		// turn on antialiasing
		smooth();
	}

	/**
	 * Processing method called once at the beginning after settings() to setup
	 * the application and instantiate necessary variables.
	 */
	public void setup()
	{
		random = new Random();
		kinectInterface = new KinectInterface(this);
		videoStreaming = new VideoStreaming(this);
		videoTrigger = new VideoTrigger(this);
		oscSocketIO = new OSCSocketIO(this);
		emotionDetector = new EmotionDetector(this);
		gui = new GUI(this);
		background(0);
	}

	/**
	 * Processing method called every frame / update cycle.
	 */
	public void draw()
	{
		detectConfigurationChange();
		
		kinectInterface.update();
		
		if(kinectInterface.getCurrentFrame() != null)
		{
//			if(EmotionDetector.detectorInitialized) {
//				emotionDetector.processFrame(kinectInterface.getCurrentFrame());
//			}
			emotionDetector.processFrame(kinectInterface.getCurrentFrame());
		}
		
		if(isVideoReceiving)
		{
			videoStreaming.receiveVideo();
		}
		
		if(videoFrameType == VideoFrameType.VIDEO_TRIGGER)
		{
			if(!videoTrigger.isCued())
			{
				videoTrigger.play();
				videoTrigger.setCued(false);
			}
			videoTrigger.drawNextFrame();
		}
		else if(videoFrameType == VideoFrameType.VIDEO_CLIP)
		{
			videoStreaming.drawVideo();
		}
		else
		{
			kinectInterface.drawImage();
		}
		
		gui.displayGUI();
		
		if(isVideoTransmitting)
		{
			videoStreaming.streamVideo();
		}
	}
	
	/**
	 * Method to detect a change in configuration in this application.
	 */
	public void detectConfigurationChange()
	{
		if(!lastConfiguration.name().equalsIgnoreCase(configuration.name()))
		{
			handleConfigurationChange();
		}
	}
	
	/**
	 * Method to handle key presses.
	 */
	public void keyPressed()
	{
		if(key == 'r' || key == 'R')
		{
			isVideoReceiving = !isVideoReceiving;
			gui.addGUIText("RECEIVING VIDEO STREAM: " + isVideoReceiving, 1000);
		}
		else if(key == 't' || key == 'T')
		{
			isVideoTransmitting = !isVideoTransmitting;
			gui.addGUIText("TRANSMITTING VIDEO STREAM: " + isVideoTransmitting, 1000);
		}
		else if(key == 'd' || key == 'D')
		{
			videoFrameType = VideoFrameType.values()[(videoFrameType.ordinal() + 1) % VideoFrameType.values().length];
			gui.addGUIText("DRAWING VIDEO FRAME TYPE: " + videoFrameType.name(), 1000);
		}
		else if(key == 'n' || key == 'N')
		{
			videoTrigger.cueNextVideoClip(random.nextBoolean());
			gui.addGUIText("PLAYING NEXT VIDEO: " + true, 1000);
		}
		else if(key == 'g' || key == 'G')
		{
			gui.toggleTestDisplayed();
		}
		else if(key == 'c' || key == 'C')
		{
			sendConfigurationChange();
		}
		else if(key == 'i' || key == 'I')
		{
			kinectInterface.toggleIsDepth();
			gui.addGUIText("KINECT INTERFACE IS DEPTH: " + kinectInterface.isDepth(), 1000);
		}
		else if(key == '1')
		{
			if(configuration != ConfigurationType.A)
			{
				configuration = ConfigurationType.A;
				handleConfigurationChange();
				gui.addGUIText("CONFIGURATION TYPE: " + configuration.name(), 1000);
			}
		}
		else if(key == '2')
		{
			if(configuration != ConfigurationType.B)
			{
				configuration = ConfigurationType.B;
				handleConfigurationChange();
				gui.addGUIText("CONFIGURATION TYPE: " + configuration.name(), 1000);
			}
		}
	}
	
	/**
	 * Methot to handle a change in configuration of this application.
	 */
	public void handleConfigurationChange()
	{
//		videoStreaming.setConfiguration(configuration);
//		oscSocketIO.setConfiguration(configuration);
		switch(configuration)
		{
			case A:
			{
				videoFrameType = VideoFrameType.VIDEO_TRIGGER;
				break;
			}
			case B:
			{
				videoFrameType = VideoFrameType.VIDEO_CLIP;
				break;
			}
			default:
			{
				videoFrameType = VideoFrameType.VIDEO_TRIGGER;
			}
		}
		lastConfiguration = configuration;
		System.out.println("Switched Configuration to " + configuration.name());
	}
	
	/**
	 * Method to set the current configuration for this application.
	 * @param configuration - The configuration to set it to.
	 */
	public static void setConfiguration(ConfigurationType configuration)
	{
		Contagion.configuration = configuration;
	}
	
	/**
	 * Method to send a configuration change to the other application.
	 */
	public void sendConfigurationChange()
	{
		ConfigurationType newConfiguration = ConfigurationType.values()[(configuration.ordinal() + 1) % ConfigurationType.values().length];
		oscSocketIO.sendMessage(newConfiguration.name());
	}
	
	/**
	 * Enum specifying the types of video frame content to display on screen.
	 * @author mikhail.jacob
	 *
	 */
	public enum VideoFrameType
	{
		VIDEO_TRIGGER,
		VIDEO_CLIP,
		VIDEO_CAMERA
	}
	
	/**
	 * Enum specifying the types of configuration for this application.
	 * @author mikhail.jacob
	 *
	 */
	public enum ConfigurationType
	{
		A,
		B
	}
}
