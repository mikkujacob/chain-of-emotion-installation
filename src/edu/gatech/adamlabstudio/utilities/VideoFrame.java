package edu.gatech.adamlabstudio.utilities;

import processing.core.PImage;

public class VideoFrame
{
	/**
	 * Object to store the video frame.
	 */
	private PImage frame;
	
	/**
	 * Timestamp of the video frame.
	 */
	private long timeStamp;
	
	/**
	 * Valence of the video frame.
	 */
	private int valence;
	
	/**
	 * Constructor to create a VideoFrame object from the video frame PImage and a timestamp.
	 * @param frame - The Video frame PImage.
	 * @param timeStamp - The timestamp.
	 */
	public VideoFrame(PImage frame, long timestamp, int valence)
	{
		this.frame = frame;
		this.timeStamp = timestamp;
		this.valence = valence;
	}

	/**
	 * @return the frame
	 */
	public PImage getFrame()
	{
		return frame;
	}

	/**
	 * @param frame the frame to set
	 */
	public void setFrame(PImage frame)
	{
		this.frame = frame;
	}

	/**
	 * @return the timeStamp
	 */
	public long getTimeStamp()
	{
		return timeStamp;
	}

	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(long timeStamp)
	{
		this.timeStamp = timeStamp;
	}
	
	/**
	 * @return the valence
	 */
	public int getValence()
	{
		return valence;
	}

	/**
	 * @param valence the valence to set
	 */
	public void setValence(int valence)
	{
		this.valence = valence;
	}
}
